# ML Font Detector

Have a Scala-MxNet-powered JSON endpoint that guesses the font used in your image, having been trained with 7 fonts.