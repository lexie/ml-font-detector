#!/bin/bash

BIN_DIR=$(dirname "$0")

cd "${BIN_DIR}/../src"
python create_training_set.py "$@"
