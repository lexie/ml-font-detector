from PIL import Image, ImageFont, ImageDraw
from random import randrange
import random

def set_to_sorted_list(s):
    res = list(s)
    res.sort()
    return res

# How random?  random position, always the word 'fog', one of three font sizes

font_families_set = set(['Arial', 'Georgia', 'Impact', 'SimSun', 'Tahoma', 'Verdana', 'Webdings'])
font_sizes = [8, 10, 24]

font_families = set_to_sorted_list(font_families_set)

fonts = {}
for family_name in font_families:
    try:
        fonts[family_name] = {
            (size): ImageFont.truetype(family_name.lower(), size=size)
            for size in font_sizes
        }
    except OSError:
        print("Cannot load all fonts for family %s" % family_name)
        try:
            del fonts[family_name]
        except KeyError:
            pass
        try:
            font_families_set.remove(family_name)
        except KeyError:
            pass

font_families = set_to_sorted_list(font_families_set)
del font_families_set

print("Remaining font families = ", repr(font_families))


def create():
    """
    Generate a random image with a single word in a random font.

    Return a tuple (feature, image), where 
        feature is the index number of the font in font_families, and 
        image is a PIL image
    """

    word = 'fog'  # TODO: randomize word
    image_width = image_height = 48
    rand_font_family_ix = random.choice(range(len(font_families)))
    rand_font_family = font_families[rand_font_family_ix]
    rand_font = fonts[rand_font_family][random.choice(font_sizes)]
    (word_width, word_height) = rand_font.getsize(word)

    # Create a grayscale image that is all white.
    image = Image.new('L', (image_width, image_height), 255)

    # Draw text on the image.
    draw = ImageDraw.Draw(image)
    rand_x = randrange(image_width - word_width)
    rand_y = randrange(image_height - word_height)
    draw.text((rand_x, rand_y), word, font=rand_font)

    return (rand_font_family_ix, image)


if __name__ == '__main__':
    # Smoke test
    (font_family_ix, image) = create()
    print("family: ", font_families[font_family_ix])
    image.show()
